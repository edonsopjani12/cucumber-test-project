$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("LoginPage.feature");
formatter.feature({
  "line": 1,
  "name": "Login Page Tests",
  "description": "",
  "id": "login-page-tests",
  "keyword": "Feature"
});
formatter.background({
  "comments": [
    {
      "line": 3,
      "value": "#  This Background executes in the beginning of every scenario on this feature file"
    }
  ],
  "line": 4,
  "name": "User navigates to the login page",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "User navigates to the login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginStepDefs.thePreconditionForTheTestGoesToWebsiteOrLogsIn()"
});
formatter.result({
  "duration": 4365250400,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "The user logs in with valid credentials",
  "description": "",
  "id": "login-page-tests;the-user-logs-in-with-valid-credentials",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 7,
      "name": "@LoginPage"
    },
    {
      "line": 7,
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "The user writes the email to AdminUsername in the email input field",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The user writes the password to AdminPassword in the password input",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "The user clicks the Login button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Check if user is logged in successfully",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "AdminUsername",
      "offset": 29
    }
  ],
  "location": "LoginStepDefs.userEnterUsername(String)"
});
formatter.result({
  "duration": 86031200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AdminPassword",
      "offset": 32
    }
  ],
  "location": "LoginStepDefs.userEnterPassword(String)"
});
formatter.result({
  "duration": 78237500,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefs.userClickLoginButton()"
});
formatter.result({
  "duration": 415405700,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefs.userCheckIsLoggedIn()"
});
formatter.result({
  "duration": 22329500,
  "status": "passed"
});
formatter.after({
  "duration": 87600,
  "status": "passed"
});
});