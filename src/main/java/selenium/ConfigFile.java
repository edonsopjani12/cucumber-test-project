package selenium;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFile {
    private static ConfigFile instance;
    private Properties properties;
    private final String resourcesPAth = "src//main//resources//";


    private ConfigFile(String fileName) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(resourcesPAth + fileName));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + resourcesPAth + fileName);
        }
    }

    public static void init(String configName) {
        if (configName == null) return;
        ConfigFile.instance = new ConfigFile(configName);
    }

    public static ConfigFile getInstance() {
        if (ConfigFile.instance == null) {
            init("configuration.properties");
        }
        return ConfigFile.instance;
    }

    //Here we get username from configuration.properties file
    public String getUsername() {
        String username = properties.getProperty("Username");
        if (username != null) return username;
        else throw new RuntimeException("Username not specified in the Configuration.properties file.");
    }

    //Here we get password from configuration.properties file
    public String getPassword() {
        String password = properties.getProperty("Password");
        if (password != null) return password;
        else throw new RuntimeException("Username not specified in the Configuration.properties file.");
    }

    //Here we get browser from configuration.properties file
    public String getBrowser() {
        String browser = properties.getProperty("browser");
        if (browser != null) return browser;
        else throw new RuntimeException("browser not specified in the Configuration.properties file.");
    }

    //Here we get application url from configuration.properties file
    public String getApplicationUrl() {
        String url = properties.getProperty("url");
        if (url != null) return url;
        else throw new RuntimeException("url not specified in the Configuration.properties file.");
    }
}
