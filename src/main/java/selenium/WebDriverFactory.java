package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import java.io.File;

public class WebDriverFactory {

    private String browserName, browserVersion;

    private WebDriverFactory() {
    }

    public WebDriver getWebDriver() {
        return driver;
    }

    private static WebDriverFactory ourInstance = new WebDriverFactory();

    public static WebDriverFactory getInstance() {
        return ourInstance;
    }

    private WebDriver driver = createWebDriver();

    private WebDriver createWebDriver() {
        WebDriver driver = null;
        switch (ConfigFile.getInstance().getBrowser().toLowerCase()) {
            case "chrome": {
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/chromedriver.exe");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("start-maximized");
                chromeOptions.addArguments("--disable-notifications");
                driver = new ChromeDriver(chromeOptions);

                browserName = ((ChromeDriver) driver).getCapabilities().getBrowserName();
                browserVersion = ((ChromeDriver) driver).getCapabilities().getVersion();
            }
            break;
            case "headless": {
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/chromedriver.exe");
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1080", "--ignore-certificate-errors");
                driver = new ChromeDriver(options);

                browserName = ((ChromeDriver) driver).getCapabilities().getBrowserName();
                browserVersion = ((ChromeDriver) driver).getCapabilities().getVersion();
            }
            break;
        }

        assert driver != null;
        System.out.println("Using browser with version: " + browserName + " " + browserVersion);
        return driver;
    }
}