package pageobjects;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.WebDriverFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class PageObjectUtils {

    public static void IsElementVisible(WebDriver driver, By by, int seconds) {

        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static void IsElementVisible(WebDriver driver, LocatorType locatorType, String value, int seconds) {

        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(getElementByLocator(locatorType, value)));
    }

    public static void WaitUntilElementIsInvisible(WebDriver driver, By by, int seconds) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }


    public static By getElementByLocator(LocatorType locatorType, String id) {
        switch (locatorType) {
            case CLASSNAME:
                return By.className(id);
            case CSS:
                return By.cssSelector(id);
            case ID:
                return By.id(id);
            case LINK:
                return By.linkText(id);
            case NAME:
                return By.name(id);
            case TAGNAME:
                return By.tagName(id);
            case XPATH:
                return By.xpath(id);
        }
        return null;
    }

    public static List<WebElement> locateElements(WebDriver webDriver, LocatorType type, String ref) {
        switch (type) {
            case ID:
                return webDriver.findElements(By.id(ref));
            case CLASSNAME:
                return webDriver.findElements(By.className(ref));
            case XPATH:
                return webDriver.findElements(By.xpath(ref));
            case CSS:
                return webDriver.findElements(By.cssSelector(ref));
            case LINK:
                return webDriver.findElements(By.linkText(ref));
            case NAME:
                return webDriver.findElements(By.name(ref));
            case TAGNAME:
                return webDriver.findElements(By.tagName(ref));
        }
        return null;
    }

    public static WebElement locateElement(WebDriver webDriver, LocatorType type, String ref) {
        switch (type) {
            case ID:
                return webDriver.findElement(By.id(ref));
            case CLASSNAME:
                return webDriver.findElement(By.className(ref));
            case XPATH:
                return webDriver.findElement(By.xpath(ref));
            case CSS:
                return webDriver.findElement(By.cssSelector(ref));
            case LINK:
                return webDriver.findElement(By.linkText(ref));
            case NAME:
                return webDriver.findElement(By.name(ref));
            case TAGNAME:
                return webDriver.findElement(By.tagName(ref));
        }
        return null;
    }

    public static By locateBy(LocatorType type, String ref) {
        switch (type) {
            case ID:
                return By.id(ref);
            case CLASSNAME:
                return By.className(ref);
            case XPATH:
                return By.xpath(ref);
            case CSS:
                return By.cssSelector(ref);
            case LINK:
                return By.linkText(ref);
            case NAME:
                return By.name(ref);
            case TAGNAME:
                return By.tagName(ref);
        }
        return null;
    }

    public enum LocatorType {
        CLASSNAME, CSS, ID, LINK, NAME, TAGNAME, XPATH
    }
}
