package pageobjects;

import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.WebDriverFactory;

import java.util.ArrayList;
import java.util.List;

public interface PageObjects {
    WebDriver driver = WebDriverFactory.getInstance().getWebDriver();

    void initializeMap();
}
