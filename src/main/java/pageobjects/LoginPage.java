package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public enum LoginPage implements PageObjects {

    //Here we put all elements we need from the page we want to automate
    InputField_EmailAddress(PageObjectUtils.LocatorType.ID, "username"),
    InputField_Password(PageObjectUtils.LocatorType.ID, "password"),
    IsLoggedIn(PageObjectUtils.LocatorType.XPATH, "//div[text()[contains(.,'You logged into a secure area!')]]"),
    LoginButton(PageObjectUtils.LocatorType.TAGNAME, "button");

    public PageObjectUtils.LocatorType locator;
    public String id;


    LoginPage(PageObjectUtils.LocatorType locator, String id) {
        this.locator = locator;
        this.id = id;
    }

    public WebElement getElement() {
        return PageObjectUtils.locateElement(driver, locator, id);
    }
    public By getBy() {
        return PageObjectUtils.locateBy(locator, id);
    }
    public List<WebElement> getElements() {
        return PageObjectUtils.locateElements(driver, locator, id);
    }

    @Override
    public void initializeMap() {

    }
}
