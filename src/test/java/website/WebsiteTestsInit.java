package website;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import selenium.ConfigFile;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import selenium.WebDriverFactory;
import stepdefs.CommonStepObjects;

@RunWith(Cucumber.class)

@CucumberOptions(monochrome = true,
        features = "src/test/java/website",
        format = {"pretty", "html:cucumber-html-reports",
                "json:cucumber-html-reports/cucumber.json"},
        glue = "stepdefs")

//This is the method that execute all tests.
public class WebsiteTestsInit {

    //This will be executed first before every test run
    @BeforeClass
    public static void InitilizeTests() {
        ConfigFile.init(System.getProperty("configName"));
        CommonStepObjects.setWebDriver(WebDriverFactory.getInstance().getWebDriver());
    }

    //This will be executed last after test run is finished
    @AfterClass
    public static void TearDown() {
        CommonStepObjects.closeDriver();
    }
}