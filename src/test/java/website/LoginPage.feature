Feature: Login Page Tests

#  This Background executes in the beginning of every scenario on this feature file
  Background: User navigates to the login page
    Given User navigates to the login page

# This is one test case it begins with Scenario( test case name) and then steps to execute that test case
  Scenario: The user logs in with valid credentials
    When The user writes the email to AdminUsername in the email input field
    And The user writes the password to AdminPassword in the password input
    And The user clicks the Login button
    Then Check if user is logged in successfully