package stepdefs.websitesteps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageobjects.LoginPage;
import selenium.ConfigFile;
import pageobjects.PageObjectUtils;
import selenium.WebDriverFactory;
import stepdefs.CommonStepObjects;

import static pageobjects.LoginPage.*;

public class LoginStepDefs extends CommonStepObjects {

    @Given("^User navigates to the login page$")
    public void thePreconditionForTheTestGoesToWebsiteOrLogsIn() throws Throwable {
        if (driver == null) driver = WebDriverFactory.getInstance().getWebDriver();
        driver.navigate().to(ConfigFile.getInstance().getApplicationUrl());
    }

    @When("^The user writes the email to (.*) in the email input field$")
    public void userEnterUsername(String email){
        PageObjectUtils.IsElementVisible(driver,InputField_EmailAddress.getBy(),15);
        InputField_EmailAddress.getElement().sendKeys(ConfigFile.getInstance().getUsername());
    }

    @And("^The user writes the password to (.*) in the password input$")
    public void userEnterPassword(String password){
        PageObjectUtils.IsElementVisible(driver,InputField_Password.getBy(),15);
        InputField_Password.getElement().sendKeys(ConfigFile.getInstance().getPassword());
    }

    @And("^The user clicks the Login button$")
    public void userClickLoginButton(){
        PageObjectUtils.IsElementVisible(driver, LoginButton.getBy(), 15);
        LoginButton.getElement().click();
    }

    @Then("^Check if user is logged in successfully$")
    public void userCheckIsLoggedIn(){
        PageObjectUtils.IsElementVisible(driver, IsLoggedIn.getBy(), 15);
    }
}