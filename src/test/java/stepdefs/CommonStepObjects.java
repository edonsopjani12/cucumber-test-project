package stepdefs;

import org.openqa.selenium.WebDriver;

public class CommonStepObjects {

    //Here we setup web driver
    protected static WebDriver driver;

    public static WebDriver setWebDriver(WebDriver webDriver) {
        driver = webDriver;
        return driver;
    }

    //This method will close driver after tests are finish
    public static void closeDriver() {
        driver.close();
    }
}